from django.db import models
import shutil
import os



class File(models.Model):
    name_of_file = models.CharField(max_length=100, unique=True)
    info = models.CharField(max_length=300)
    file = models.FileField(upload_to='files/')
    cover = models.ImageField(upload_to='files/', null=True, blank=True)



    def __str__(self):
        return self.name_of_file

    def delete(self, *args, **kwargs):
        self.file.delete()
        self.cover.delete()
        super().delete(*args, **kwargs)

    def move_cover(self, *args, **kwargs):
        src = self.cover.path
        txt = src
        dst = txt.replace("/media/files/", "/media/pub/")
        shutil.move(src, dst)

    def move_file(self, *args, **kwargs):
        src = self.file.path
        txt = src
        dst = txt.replace("/media/files/", "/media/pub/")
        shutil.move(src, dst)
